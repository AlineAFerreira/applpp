import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {LoginScreen} from './src/pages/login/LoginPage';
import {SigUpScreen} from './src/pages/signup/SignUpPage';
import {HomeScreen} from './src/pages/home/HomePage';
import {RootStackParamList} from './src/Types/index';

const Stack = createStackNavigator<RootStackParamList>();

function App(): JSX.Element {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{title: 'Entrar'}}
        />
        <Stack.Screen
          name="SignUp"
          component={SigUpScreen}
          options={{title: 'Vamos lá', headerShown: false}}
        />
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{title: 'Início'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
