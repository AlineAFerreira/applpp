/* eslint-disable no-undef */
import {createStackNavigator} from '@react-navigation/stack';
import {LoginPage} from '../pages/login/LoginPage';

const AppNavigator = createStackNavigator(
  {
    Login: LoginPage,
  },
  {
    initialRouteName: 'Login',
  },
);

export default (Router = createAppContainer(AppNavigator));
