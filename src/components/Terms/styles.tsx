import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  boxTerms: {
    flex: 1,
    width: '100%',
    paddingTop: 75,
  },

  boxStep: {
    width: '100%',
    padding: 15,
  },

  titleTerms: {
    fontSize: 20,
    textTransform: 'uppercase',
    color: '#683592',
    fontWeight: 'bold',
  },

  textTerms: {
    marginTop: 20,
    fontSize: 18,
    lineHeight: 26,
    paddingBottom: 150,
  },
});
