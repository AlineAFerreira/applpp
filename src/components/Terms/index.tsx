import React from 'react';
import {Text} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {styles} from './styles';

export const Terms: React.FC = (): JSX.Element => {
  return (
    <ScrollView style={[styles.boxStep, styles.boxTerms]}>
      <Text style={styles.titleTerms}>Termos e condições</Text>
      <Text style={styles.textTerms}>
        Considerando que o Sport Club Corinthians Paulista, é uma entidade de
        prática desportiva com sede nesta capital na Rua São Jorge, nº 777,
        Bairro de Tatuapé, inscrita no CNPJ/MF sob o nº 61.092.722/0001- 26,
        doravante denominada simplesmente “Clube” ou “Corinthians”, na qualidade
        de legítima e única detentora da propriedade e uso exclusivo dos
        símbolos do Corinthians, assim compreendidos sua denominação, seu
        emblema, marca, mascote e demais sinais distintivos nos exatos termos do
        Artigo nº 87 da Lei nº 9.615/98 e alterações posteriores, sendo entidade
        de prática desportiva devidamente filiada às entidades de administração
        do desporto da modalidade Futebol e participante de competições
        esportivas válidas por campeonatos oficiais de futebol profissional, com
        a cobrança de ingressos. O BANCO DE PONTOS FIDELIDADE S.A., sociedade
        anônima com sede na Avenida Maria Coelho Aguiar, nº 215, bloco A, 1º
        andar, conjunto A2, na Cidade de São Paulo, Estado de São Paulo,
        devidamente inscrita no CNPJ/MF sob nº 18.653.639/0001-31 criou e
        desenvolveu um Programa de Fidelidade voltado exclusivamente aos seus
        torcedores do Corinthians denominado “Loucos por Pontos”, doravante
        denominado “Loucos por Pontos” ou “Programa”, o qual oferece diversas
        formas de obtenção de pontuação devidamente descritas nestes Termos e
        Condições de Uso da Plataforma Louco por Pontos (“Termo”), que visa a
        adesão e participação no Programa.
      </Text>
    </ScrollView>
  );
};
