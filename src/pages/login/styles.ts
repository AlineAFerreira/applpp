import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  align-items: center;
  justify-content: flex-start;
`;

export const Logo = styled.Image.attrs({
  resizeMode: 'cover',
  source: {
    uri:
      'https://bpfportal.blob.core.windows.net/images/header_corinthians/logoHeaderBlack.png',
  },
})`
  width: 220px;
  height: 80px;
  background-color: transparent;
  margin-top: 20;
`;

export const InputText = styled.TextInput`
  font-size: 22px;
  width: 90%;
  height: 50px;
  margin-top: 30px;
  padding: 10px;
  font-weight: 600;
  border-bottom-width: 1px;
`;

export const ForgotPassword = styled.Text`
  color: #522974;
  font-weight: 300;
  text-transform: uppercase;
  font-size: 18;
  margin-top: 30;
`;

export const BtnSignIn = styled.TouchableHighlight.attrs({
  underlayColor: '#999',
})`
  background-color: #000;
  border-radius: 50px;
  width: 95%;
  padding: 15px;
  margin: 20px;
`;

export const BtnSignUp = styled.TouchableHighlight.attrs({
  underlayColor: '#8f60b5',
})`
  background-color: #683592;
  border-radius: 50px;
  width: 95%;
  padding: 15px;
  margin: 20px;
`;

export const BtnText = styled.Text`
  color: #fff;
  text-transform: uppercase;
  text-align: center;
  font-weight: bold;
  font-size: 20px;
`;
