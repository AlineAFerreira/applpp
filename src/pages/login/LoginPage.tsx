import React from 'react';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootStackParamList} from '../../Types';
import {Container, Logo, InputText, ForgotPassword, BtnSignIn, BtnSignUp, BtnText} from './styles';

type LoginScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Login'
>;

type Props = {
  navigation: LoginScreenNavigationProp;
};

export const LoginScreen = (props: Props) => {
  const {navigation} = props;
  return (
    <Container>
      <Logo/>
      <InputText
        placeholder="CPF ou E-mail"
      />
      <InputText
        secureTextEntry={true}
        placeholder="Digite a senha"
      />

      <ForgotPassword>Esqueci Minha senha!</ForgotPassword>

      <BtnSignIn onPress={() => navigation.navigate('Home')}>
        <BtnText>Entrar</BtnText>
      </BtnSignIn>

      <BtnSignUp onPress={() => navigation.navigate('SignUp')}>
        <BtnText>Cadastrar</BtnText>
      </BtnSignUp>
    </Container>
  );
};
