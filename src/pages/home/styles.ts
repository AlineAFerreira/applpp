import styled from 'styled-components/native';

export const Container = styled.View``;

export const MainBanner = styled.Image.attrs({
  source: {
    uri: 'https://cdn1.bpfidelidade.com.br/images/bannerprincipal_min.png',
  },
})`
  width: 100%;
  height: 160px;
`;
