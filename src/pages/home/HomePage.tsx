import React, {ReactElement} from 'react';
import {View, Image, FlatList} from 'react-native';
import {Container, MainBanner} from './styles';

export const HomeScreen: React.FC = (): ReactElement => {
  return (
    <Container>
      <MainBanner
        source={{
          uri:
            'https://cdn1.bpfidelidade.com.br/images/bannerprincipal_min.png',
        }}
      />
    </Container>
  );
};
