import React, {useState} from 'react';
import {RootStackParamList} from '../../Types';
import {StackNavigationProp} from '@react-navigation/stack';
import IonIcon from 'react-native-vector-icons/Ionicons';
import {Terms} from '../../components/Terms';

import {
  Container,
  BtnBack,
  BtnBackText,
  BoxStep,
  LabelInput,
  TextInput,
  PasswordInput,
  BtnAction,
  BtnActionText,
} from './styles';

type LoginScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'SignUp'
>;

type Props = {
  navigation: LoginScreenNavigationProp;
};

export const SigUpScreen = (props: Props) => {
  const [currentStep, setCurrentStape] = useState(1);

  const prevStep = (): void => {
    let currentStep2 = currentStep;
    currentStep2 = currentStep2 - 1;
    setCurrentStape(currentStep2);
  };

  const nextStep = (): void => {
    let currentStep1 = currentStep;
    currentStep1 = currentStep1 + 1;
    setCurrentStape(currentStep1);
  };

  const prevButton = (navigation: any): JSX.Element => {
    let currentStep2 = currentStep;
    return (
      <BtnBack
        onPress={
          currentStep2 === 1 ? () => navigation.navigate('Login') : prevStep
        }>
        <IonIcon name="ios-arrow-back" size={30} color="#FFF" />
        <BtnBackText>Voltar </BtnBackText>
      </BtnBack>
    );
  };

  const nextButton = (navigation: any) => {
    let currentStep1 = currentStep;
    if (currentStep1 < 6) {
      return (
        <BtnAction onPress={nextStep}>
          <BtnActionText>Continuar</BtnActionText>
        </BtnAction>
      );
    } else {
      return (
        <BtnAction onPress={() => navigation.navigate('Home')}>
          <BtnActionText>Aceitar</BtnActionText>
        </BtnAction>
      );
    }
  };

  const {navigation} = props;
  return (
    <Container>
      {prevButton(navigation)}
      <Step1 currentStep={currentStep} />
      <Step2 currentStep={currentStep} />
      <Step3 currentStep={currentStep} />
      <Step4 currentStep={currentStep} />
      <Step5 currentStep={currentStep} />
      <Step6 currentStep={currentStep} />

      {nextButton(navigation)}
    </Container>
  );
};

function Step1(props: {currentStep: number}) {
  if (props.currentStep !== 1) {
    return null;
  }
  return (
    <BoxStep>
      <LabelInput>CPF:</LabelInput>
      <TextInput placeholder="000.000.000-00" />
    </BoxStep>
  );
}

function Step2(props: {currentStep: number}) {
  if (props.currentStep !== 2) {
    return null;
  }
  return (
    <BoxStep>
      <LabelInput>Nome completo:</LabelInput>
      <TextInput placeholder="Digite seu nome" />

      <LabelInput>Data de Nascimento:</LabelInput>
      <TextInput placeholder="DD/MM/AAAA" />
    </BoxStep>
  );
}

function Step3(props: {currentStep: number}) {
  if (props.currentStep !== 3) {
    return null;
  }
  return (
    <BoxStep>
      <LabelInput>E-mail:</LabelInput>
      <TextInput placeholder="Digite seu e-mail" />

      <LabelInput>Telefone:</LabelInput>
      <TextInput placeholder="(00)0000-0000" />
    </BoxStep>
  );
}

function Step4(props: {currentStep: number}) {
  if (props.currentStep !== 4) {
    return null;
  }
  return (
    <BoxStep>
      <LabelInput>Senha:</LabelInput>
      <PasswordInput placeholder="Digite sua senha" />

      <LabelInput>Confirme a Senha:</LabelInput>
      <PasswordInput placeholder="Confirme a senha" />
    </BoxStep>
  );
}

function Step5(props: {currentStep: number}) {
  if (props.currentStep !== 5) {
    return null;
  }
  return (
    <BoxStep>
      <LabelInput>CEP:</LabelInput>
      <TextInput placeholder="00000-000" />
    </BoxStep>
  );
}

function Step6(props: {currentStep: number}) {
  if (props.currentStep !== 6) {
    return null;
  }
  return <Terms />;
}
