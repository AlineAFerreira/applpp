import styled from 'styled-components/native';
import {getStatusBarHeight, getBottomSpace} from 'react-native-iphone-x-helper';

export const Container = styled.View`
  flex: 1;
  height: 100%;
  align-items: flex-start;
  justify-content: center;
`;

export const BtnBack = styled.TouchableOpacity`
  background-color: #683592;
  padding: ${getStatusBarHeight()}px 20px;
  position: absolute;
  top: 0;
  z-index: 2;
  width: 100%;
  flex-direction: row;
  align-items: center;
`;

export const BtnBackText = styled.Text`
  color: #fff;
  font-size: 20px;
  text-transform: uppercase;
  margin-left: 25px;
`;

export const BoxStep = styled.View`
  width: 100%;
  padding: 15px;
`;

export const LabelInput = styled.Text`
  color: #828282;
  font-size: 18px;
  margin-top: 20px;
`;

export const TextInput = styled.TextInput.attrs({
  placeholderTextColor: '#a8a8a8',
})`
  font-size: 22;
  color: #a8a8a8;
  width: 100%;
  height: 50px;
  padding: 10px;
  font-weight: 600;
  border-bottom-width: 2px;
`;

export const PasswordInput = styled.TextInput.attrs({
  placeholderTextColor: '#a8a8a8',
  secureTextEntry: true,
})`
  font-size: 22;
  color: #a8a8a8;
  width: 100%;
  height: 50px;
  padding: 10px;
  font-weight: 600;
  border-bottom-width: 2px;
`;

export const BtnAction = styled.TouchableHighlight.attrs({
  underlayColor: '#fff',
})`
  background-color: #683592;
  width: 100%;
  padding: 15px;
  padding-bottom: ${getBottomSpace}px;
  position: absolute;
  bottom: 0;
`;

export const BtnActionText = styled.Text`
  color: #fff;
  text-transform: uppercase;
  text-align: center;
  font-weight: bold;
  font-size: 20px;
`;
